tot = 0
for i in range(1000):
	if (i%3 == 0 or i % 5 == 0):
		tot = tot + i
print ("Multiples of 3 and 5: ")
print (tot)

a, b = 0,1
tot = 0
while True:
	a, b = b, a + b
	if b >= 4000000:
		break
	if b % 2 == 0:
		tot += b
print ("Even Fibonacci numbers: ")
print (tot)

x = 600851475143
i = 2
while i * i < x:
	while x % i == 0:
		x = x / i
	i = i + 1
print("Largest prime factor: ")
print(x)
